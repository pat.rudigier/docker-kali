# Create container

./buildKaliContainer.sh

# Bash into container

docker exec kaliLinux bash

# Run init script

./initscript

# Work with Kali

Now you have a clean install of Kali linux which can be entered via Remote Desktop.

You need to install all tools you need yourself. See for help:
