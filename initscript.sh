#!/bin/bash

# install VNC Server and GUI
apt -y update && apt -y upgrade && apt -y autoremove && apt clean

apt -y install kali-desktop-xfce tightvncserver

echo "Setting VNC Password..."
vncpasswd 

echo "Starting VNC Server..."
export USER=root
tightvncserver :0 -geometry 1280x800 -depth 32 -pixelformat rgb565
echo "VNC Server started"

