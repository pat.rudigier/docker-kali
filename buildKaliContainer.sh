#!/bin/bash

sudo docker build -t my-kali-linux-image .
docker run -t -d --name kaliLinux -p 25900:5900 -p 25901:5901 -v container-data:/root/data/ my-kali-linux-image

